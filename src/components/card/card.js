/**
 * This is card for individual entry
 */

import React, { Component } from "react";
import moment from "moment";
import {
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardText
} from "material-ui/Card";
import { FlatButton, Avatar, Chip, TextField } from "material-ui";
import SvgIconFavorite from "material-ui/svg-icons/action/favorite";
import SvgIconCommunicationChat from "material-ui/svg-icons/communication/chat";
import { red500 } from "material-ui/styles/colors";

const style = {
  chip: { margin: 4 },
  input: { width: "65%" }
};

const getLikes = likes => (
  <Chip style={style.chip}>
    <Avatar icon={<SvgIconFavorite color={red500} />} />
    Likes: {likes}
  </Chip>
);

const getComments = comments => (
  <Chip style={style.chip}>
    <Avatar icon={<SvgIconCommunicationChat />} />
    comments: {comments}
  </Chip>
);

export default class CardItem extends Component {
  state = { comment: "" };

  onChange = (e, comment) => this.setState({ comment });

  onComment = () => {
    const { onComment, index } = this.props;
    const { comment } = this.state;
    onComment({ comment, id: index });
    this.setState({ comment: "" });
  };

  render() {
    const {
      Image,
      likes = 0,
      comments = [],
      desc = "",
      index,
      onLike,
      onRemove,
      timestamp
    } = this.props;
    const { comment } = this.state;
    return (
      <Card className="card">
        <CardHeader
          title="Harish"
          subtitle={moment(timestamp).format("DD-MM-YYYY")}
          avatar="https://pbs.twimg.com/profile_images/1002207419953377280/GY4Dwbvo_400x400.jpg"
        />
        <CardMedia
          overlay={
            <div className="overLay">
              {getLikes(likes)}
              {getComments(comments.length)}
            </div>
          }
        >
          <img src={Image} />
        </CardMedia>
        {desc && <CardText>{desc}</CardText>}
        <CardActions>
          <FlatButton label="Like" secondary onClick={() => onLike(index)} />
          <FlatButton label="Remove Card" onClick={() => onRemove(index)} />
        </CardActions>
        <CardText>
          <div>
            <span className="bold">Comments:</span>
            {comments.map(str => (
              <div className="comments">{str}</div>
            ))}
          </div>
          <div>
            <TextField
              id={`comment_${index}`}
              hintText="Enter the comment"
              multiLine
              value={comment}
              onChange={this.onChange}
              style={style.input}
            />
            <FlatButton
              label="comment"
              primary
              disabled={comment === ""}
              onClick={this.onComment}
              className="comment-btn"
            />
          </div>
        </CardText>
      </Card>
    );
  }
}
