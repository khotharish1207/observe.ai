/**
 * Image Upload dialog
 */

import React, { Component } from "react";
import { Dialog, FlatButton, TextField } from "material-ui";
import ImageUploader from "react-images-upload";
import moment from "moment";

export default class Upload extends Component {
  state = { imageUrl: "", desc: "" };

  onDrop = (pictureFiles, url) => {
    console.log("pictureFiles", pictureFiles);
    this.setState({ imageUrl: url[0] });
  };

  onChange = (e, desc) => this.setState({ desc });

  onSubmit = () => {
    const { imageUrl, desc } = this.state;
    const { onSubmit, onClose } = this.props;
    const obj = {
      Image: imageUrl,
      likes: 0,
      timestamp: moment(),
      comments: [],
      desc
    };
    onSubmit([obj]);
    onClose();
  };

  render() {
    const { open = false, onClose } = this.props;
    const { imageUrl, desc } = this.state;
    const actions = [
      <FlatButton label="Cancel" primary onClick={onClose} />,
      <FlatButton
        label="Submit"
        primary
        keyboardFocused
        onClick={this.onSubmit}
        disabled={imageUrl === "" || desc === ""}
      />
    ];
    return (
      <Dialog
        title="Upload Image"
        actions={actions}
        open={open}
        onRequestClose={onClose}
        bodyStyle={{ overflow: "auto" }}
      >
        <div className="modal-body">
          <TextField
            floatingLabelText="Enter the description"
            onChange={this.onChange}
            value={desc}
            multiLine
            fullWidth
          />
          <ImageUploader
            withIcon={true}
            buttonText="Choose images"
            onChange={this.onDrop}
            imgExtension={[".jpg", ".gif", ".png", ".gif", ".jpeg"]}
            maxFileSize={5242880}
            singleImage
            withPreview
            disabled
          />
        </div>
      </Dialog>
    );
  }
}
