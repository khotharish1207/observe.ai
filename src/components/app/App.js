/**
 * This is the entry of app
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AppBar from "material-ui/AppBar";
import FloatingActionButton from "material-ui/FloatingActionButton";
import ContentAdd from "material-ui/svg-icons/content/add";

import Card from "../card/card";
import Upload from "../uploadDialog/uploadDialog";
import {
  fetchImages,
  setData,
  addLike,
  removeCard,
  addComment
} from "../../redux/actions";
import "./App.css";
import "../card/card.css";

class App extends Component {
  state = {
    isOpen: false
  };
  componentDidMount = () => {
    this.props.fetchImages();
  };

  openAddModal = () => this.setState({ isOpen: true });
  
  closeAddModal = () => this.setState({ isOpen: false });

  render() {
    const {
      images: { data = [] },
      setData,
      addLike,
      removeCard,
      addComment
    } = this.props;
    const { isOpen } = this.state;
    return (
      <div className="App">
        <AppBar title="Observe.AI" />

        <FloatingActionButton className="add-icon" onClick={this.openAddModal}>
          <ContentAdd />
        </FloatingActionButton>

        <div className="image-list list">
          {data.map((item, index) => (
            <Card
              key={index}
              {...item}
              index={index}
              onLike={addLike}
              onRemove={removeCard}
              onComment={addComment}
            />
          ))}
        </div>

        <Upload onClose={this.closeAddModal} open={isOpen} onSubmit={setData} />
      </div>
    );
  }
}

const mapStateToProps = ({ images }) => ({
  images
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { fetchImages, setData, addLike, removeCard, addComment },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
