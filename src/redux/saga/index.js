import { takeEvery, put } from "redux-saga/effects";
import axios from "axios";
import { FETCH_IMAGES, setData, setError } from "../actions";

const dataUrl = "http://starlord.hackerearth.com/insta";

function* fetchImages(action) {
  try {
    const resp = yield axios.get(dataUrl, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS"
      }
    });
    const { data } = resp;

    yield put(setData(data));
  } catch (error) {
    console.log("fetch image error");
    yield put(setError(true));
  }
}

function* rootSaga() {
  yield takeEvery(FETCH_IMAGES, fetchImages);
}

export default rootSaga;
