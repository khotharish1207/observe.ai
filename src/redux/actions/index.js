/**
 * Redux ations needed for data manipulation
 */

import { createAction } from "redux-actions";

export const FETCH_IMAGES = "FETCH_IMAGES";
export const fetchImages = createAction(FETCH_IMAGES);

export const SET_DATA = "SET_DATA";
export const setData = createAction(SET_DATA);

export const SET_ERROR = "SET_ERROR";
export const setError = createAction(SET_ERROR);

export const ADD_LIKE = "ADD_LIKE";
export const addLike = createAction(ADD_LIKE);

export const REMOVE_CARD = "REMOVE_CARD";
export const removeCard = createAction(REMOVE_CARD);

export const ADD_COMMENT = "ADD_COMMENT";
export const addComment = createAction(ADD_COMMENT);
