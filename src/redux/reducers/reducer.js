/**
 * Reducer to update redux store depends on action dispatched for cards
 */

import {
  SET_DATA,
  SET_ERROR,
  ADD_LIKE,
  REMOVE_CARD,
  ADD_COMMENT
} from "../actions";

const data = [
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_1b956cd6.jpg",
    likes: 540,
    timestamp: "2017-04-12 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_2c611827.jpeg",
    likes: 270,
    timestamp: "2017-04-13 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_3d151734.jpg",
    likes: 380,
    timestamp: "2017-04-14 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_4dccf2bf.jpeg",
    likes: 400,
    timestamp: "2017-04-15 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_6ea7773c.jpeg",
    likes: 752,
    timestamp: "2017-04-16 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_7f2d4157.jpeg",
    likes: 681,
    timestamp: "2017-04-17 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_8fb860e0.jpeg",
    likes: 525,
    timestamp: "2017-04-18 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_9041974b.jpeg",
    likes: 728,
    timestamp: "2017-04-19 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_100ccb04a.jpeg",
    likes: 662,
    timestamp: "2017-04-20 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_52923b48.jpeg",
    likes: 279,
    timestamp: "2017-04-21 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_1131f79ff.jpeg",
    likes: 347,
    timestamp: "2017-04-22 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_123a00217.jpeg",
    likes: 672,
    timestamp: "2017-04-23 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_1344b7582.jpeg",
    likes: 260,
    timestamp: "2017-04-24 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_144f684d4.jpeg",
    likes: 881,
    timestamp: "2017-04-25 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_155b9c1ee.jpeg",
    likes: 993,
    timestamp: "2017-04-26 14:23:45"
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_16795b053.jpeg",
    likes: 1024,
    timestamp: "2017-04-27 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_17a21aad9.jpeg",
    likes: 729,
    timestamp: "2017-04-28 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_1878832d3.jpeg",
    likes: 140,
    timestamp: "2017-04-29 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_1980e24ae.jpeg",
    likes: 760,
    timestamp: "2017-04-30 14:23:45",
    comments: []
  },
  {
    Image:
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/insta_20897a83a.jpeg",
    likes: 669,
    timestamp: "2017-05-01 14:23:45",
    comments: []
  }
];

const sortByDate = arr =>
  arr.sort((a, b) => {
    return new Date(b.timestamp) - new Date(a.timestamp);
  });

const initialState = {
  isError: false,
  data: sortByDate(data)
};

const reducer = (state = initialState, { type, payload }) => {
  let data = [];
  switch (type) {
    case SET_DATA:
      return { ...state, data: sortByDate([...payload, ...state.data]) };
    case SET_ERROR:
      return { ...state, isError: payload };
    case ADD_LIKE:
      data = state.data.map((item, index) =>
        payload === index ? { ...item, likes: item.likes + 1 } : item
      );
      return { ...state, data: sortByDate(data) };
    case REMOVE_CARD:
      return {
        ...state,
        data: sortByDate(state.data.filter((item, index) => payload !== index))
      };

    case ADD_COMMENT:
      const { comment, id } = payload;
      data = state.data.map((item, index) =>
        id === index ? { ...item, comments: [...item.comments, comment] } : item
      );
      return { ...state, data: sortByDate(data) };
    default:
      return state;
  }
};

export default reducer;
